#include <stdio.h>
#include <stdlib.h>

int get_count(int *results, int total, int coin_amt)
{
    int remainder = total - coin_amt;
    if (remainder == 0 || remainder == 1 || coin_amt == 1) return 1;
    else return results[remainder - 1];
}

int main()
{
    int coins[] = { 200, 100, 50, 20, 10, 5, 2, 1 };

    int total = 10;
    int *found_results = (int *)malloc(total * sizeof(int));

    for(int i = 0; i < total; ++i)
    {
        found_results[i] = 0;
    }

    for(int i = 0; i < total; ++i)
    {
        int sub_total = 0;
        for(int c = 0; c < 8; ++c)
        {
            if(coins[c] <= i + 1)
            {
                sub_total += get_count(found_results, i + 1, coins[c]);
            }
        }
        found_results[i] = sub_total;
    }

    for(int i = 0; i < total; ++i)
    {
        printf("%d, ", found_results[i]);
    }
    printf("\n");

    printf("%d\n", found_results[total - 1]);

    free(found_results);
    return 0;
}
