#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>

uint64_t find_lpf(uint64_t n)
{
    uint64_t result = 0;

    // divide out all 2's
    while (n % 2 == 0)
    {
        n = n / 2;
        result = 2;
    }

    // divide out all 3's
    while (n % 3 == 0)
    {
        n = n / 3;
        result = 3;
    }

    // divide out all 6*n-1 and 6*n+1 (might be primes)
    uint64_t potential_prime = 5;
    while(potential_prime <= n)
    {
        if (n % potential_prime == 0) result = potential_prime;
        // divide out the potential_prime
        while(n % potential_prime == 0)
        {
            n = n / potential_prime;
        }
        potential_prime += 2;

        if (n % potential_prime == 0) result = potential_prime;
        while(n % potential_prime == 0)
        {
            n = n / potential_prime;
        }
        potential_prime += 4;
    }

    return result;
}

int main()
{
    uint64_t n = 600851475143;

    uint64_t answer = find_lpf(n);
    printf("%" PRIu64 "\n", answer);
}

