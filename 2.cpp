#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

uint32_t fib(int n, uint32_t *values)
{
    if (!values[n])
        values[n] = fib(n - 1, values) + fib(n - 2, values);
    return values[n];
}

int main()
{
    uint64_t answer = 0;

    uint32_t *values = (uint32_t *)malloc(4 * 1000);
    values[0] = 1;
    values[1] = 1;

    int n = 2;
    while(true)
    {
        uint32_t fibN = fib(n, values);
        n++;

        if (fibN % 2 == 0)
            answer += fibN;

        if (fibN > 4000000) break;
    }
    printf("%lu\n", answer);

    free(values);
    return 0;
}
