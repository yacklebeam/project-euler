#include <stdint.h>
#include <stdio.h>

int main()
{
    uint32_t answer = 0;
    for (uint16_t i = 1; i < 1000; ++i)
    {
        if (!(i % 3 && i % 5))
            answer += i;
    }
    printf("%u\n", answer);
    return 0;
}
